<div align="center">

<p align="center">
  <a href="" rel="noopener">
    <img width=200px height=200px src="https://upload.wikimedia.org/wikipedia/commons/2/24/Ansible_logo.svg" alt="Ansible logo"></a>
  </a>
</p>

  [![Status](https://img.shields.io/badge/status-Active-success.svg)]() 
  [![Ansible Galaxy](https://img.shields.io/ansible/role/TODO)]() 
  [![Ansible Version](https://img.shields.io/badge/ansible-TODO-informational)]() 
  [![Ansible Version](https://img.shields.io/ansible/quality/TODO)]() 
  [![License](https://img.shields.io/badge/license-GPLv3-blue.svg)](/LICENSE)

</div>

---

# System Cleanup

Ansible role that performs cleanup tasks like cleaning up cache, removing old logs, etc.

At the momment this role performs the following actions:

- Removes unecesary packages.
- Autoremoves unecesary dependencies.
- Autocleans package cache.
- Prunes docker data.
- Remove unecesary users and groups.
- Cleanup thumbnails cache.

## 🦺 Requirements

- Python Apt module. `python3-apt`

## 🗃️ Role Variables

*TODO*

## 📦 Dependencies

*TODO*

## 🤹 Example Playbook

*TODO*

## ⚖️ License

This code is released under the [GPLv3](./LICENSE) license. For more information refer to `LICENSE.md`

## ✍️ Author Information

Gerson Rojas <thegrojas@protonmail.com>
